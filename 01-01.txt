a) Richtig. Der Ablauf eines deterministischen Algorithmus ist bei
gleichen Eingaben immer gleich, also ist auch sein Ergebnis bei gleichen
Eingaben immer gleich (determiniert).

b) Falsch. Bspw. ist a = 0 ; b = 0 ; a = 1 || b = 2 nichtsequentiell,
also nichtdeterministisch, aber das Ergebnis ist immer gleich
(determiniert).

c) Richtig. -- Kontraposition der Aussage: Jeder nichtsequentielle
Algorithmus ist nichtdeterministisch.

d) Falsch. Siehe b.

e) Denke denke.
