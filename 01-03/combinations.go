/**
 * Go program for solving the 3. exercise of the first exercise sheet of ALP4
 */
package 01-03

import "fmt"

// the mergesort from the book

func merge (a []int, m int) {
    n, b:= len(a), make([]int, len(a))
    for j, i, k:= 0, 0, m; j < n; j++ {
        if i < m && k < n {
            if a[i] < a[k] {
                b[j] = a[i]; i++
            } else {
                b[j] = a[k]; k++
            }
        } else if i < m {
            b[j] = a[i]; i++
        } else if k < n {
            b[j] = a[k]; k++
        }
    }
    copy (a, b)
}

func mergesort (a []int, e chan bool) {
    if len (a) > 1 {
        m:= len(a) / 2
        c, d:= make(chan bool), make (chan bool)
        go mergesort(a[:m], c)
        go mergesort(a[m:], d)
        <-c; <-d
        merge(a, m)
    }
    e <- true
}

// a function for generating all permutations of the given slice of integers
// following the algorithm of Narayana Pandita

func perms (intslice []int) [][]int {
    // create a slice of slices for the storing of the permutations
    permslice := make([][]int, Factorial(len(intslice)))
    for i := range permslice {
        permslice[i] = make([]int, len(intslice))
    }
    // sort the given slice with mergesort
    done:= make(chan bool)
    go mergesort (intslice, done)
    <-done
    // add it as first permutation
    for r := range intslice {
        permslice[0][r]=intslice[r]
    }



    // Now repeating the following steps, till all permutations are found:
    end := true
    counter := 1
    for end {

        // First:Find the largest index k such that intslice[k] < intslice[k +
        // 1]. If no such index exists, the permutation is the last permutation.
        var resk, resl int = -1, -1

        for k := 0; k < len(intslice)-1; k++ {
            if intslice[k] < intslice[k + 1] {
                resk = k
            }
            //fmt.Println("k = ",resk)
        }
        if resk==-1 {
            end = false
        } else {
            // Second: Find the largest index l such that intslice[k] <
            // intslice[l]. Since k + 1 is such an index, l is well defined and
            // satisfies k < l.
            for l := 0; l < len(intslice); l++ {
                if intslice[resk] < intslice[l] {
                    resl = l
                }
                //fmt.Println("l = ",resl)
            }

            // Third: Swap intslice[k] with intslice[l].
            temp := intslice[resk]
            intslice[resk] = intslice[resl]
            intslice[resl] = temp

            // Forth: Reverse the sequence from intslice[k + 1] up to and
            // including the final element intslice[n].
            revslice := make([]int, len(intslice) - (resk + 1))
            for m := resk + 1; m < len(intslice); m++ {
                revslice[m - (resk + 1)] = intslice[m]
            }
            for i, j := 0, len(revslice)-1; i < j; i, j = i+1, j-1 {
                revslice[i], revslice[j] = revslice[j], revslice[i]
            }
            for n := resk + 1; n < len(intslice); n++ {
                intslice[n] = revslice[n - (resk + 1)]
            }
            for o := range intslice {
                permslice[counter][o]=intslice[o]
            }
            counter++
        }
    }
    return permslice
}

// a simple factorial function in go

func Factorial(x int) int {
    if x == 0 {
        return 1
    }
    return x * Factorial(x - 1)
}

func main() {
    arr := [5]int{1,2,3,4,5}
    a := arr[:]
    permutations := perms(a)

    for i := range permutations {
        var x, y uint = 0, 0
        for j := range permutations[i] {
            var operation int = permutations[i][j]
            switch operation {
            case 1:
                x++
            case 2:
                y++
            case 3:
                y += 2
            case 4:
                y = x + 2
            default:
                x = y + 3
            }
        }
        fmt.Println("x = ", x, "  y = ", y, "  Reihenfolge: ", permutations[i])
    }
}
